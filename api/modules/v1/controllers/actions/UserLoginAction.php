<?php


namespace api\modules\v1\controllers\actions;

use common\helpers\GetErrorsHelpers;
use Yii;
use yii\web\HttpException;
use common\models\forms\LoginForm;

/**
 * Class UserLoginAction
 * @package api\modules\v1\controllers\actions
 */
class UserLoginAction extends \yii\base\Action
{
    public function run()
    {
        $model = new LoginForm();
        $model->scenario = LoginForm::SCENARIO_USER;
        if ($model->load(Yii::$app->request->bodyParams, '')){
            if (!$model->login()){
                throw new HttpException(400, GetErrorsHelpers::getError($model->getErrors()));
            }
            return [
                'data' => $model
            ];
        }
        throw new HttpException(400, 'Невірні данні');
    }
}
