<?php

namespace api\modules\v1\controllers\actions;

use Yii;

/**
 * Class UserLogoutAction
 * @package api\modules\v1\controllers\actions
 */
class UserLogoutAction extends \yii\base\Action
{
    public function run()
    {
        $user = Yii::$app->user->identity;
        if (!empty($user)){
            $user->generateAccessToken();
            $user->fcm_token = null;
        }
        return ['data' => $user->save()];
    }
}
