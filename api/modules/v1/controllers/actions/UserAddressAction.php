<?php


namespace api\modules\v1\controllers\actions;

use common\helpers\GetErrorsHelpers;
use common\models\UserAddress;
use Yii;
use yii\web\HttpException;

/**
 * Class UserCreateAction
 * @package api\modules\v1\controllers\actions
 */
class UserAddressAction extends \yii\base\Action
{
    public function run()
    {
        $user_id = Yii::$app->user->getId();
        $model = UserAddress::findOne(['user_id' => $user_id]);
        if (empty($model)){
            $model = new UserAddress();
            $model->user_id = $user_id;
        }
        if ($model->load(Yii::$app->request->bodyParams, '')){
            if (!$model->save()){
                throw new HttpException(400, GetErrorsHelpers::getError($model->getErrors()));
            }
            return [
                'data' => $model
            ];
        }

        throw new HttpException(400, 'Невірні данні');
    }
}
