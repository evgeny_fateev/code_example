<?php


namespace api\modules\v1\controllers\actions;

use common\helpers\GetErrorsHelpers;
use Yii;
use common\models\forms\SignupForm;
use yii\web\HttpException;

/**
 * Class UserCreateAction
 * @package api\modules\v1\controllers\actions
 */
class UserCreateAction extends \yii\base\Action
{
    public function run()
    {
        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_USER]);

        if ($model->load(Yii::$app->request->bodyParams, '')){
            if (!$model->registration()){
                throw new HttpException(400, GetErrorsHelpers::getError($model->getErrors()));
            }
            return [
                'data' => $model
            ];
        }

        throw new HttpException(400, 'Невірні данні');
    }
}
