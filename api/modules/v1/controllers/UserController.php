<?php

namespace api\modules\v1\controllers;


use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class UserController
 * @package api\modules\v1\controllers
 */
class UserController extends ActiveController
{
	public $modelClass = 'common\models\User';
	public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
	{
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
			'class' => 'yii\filters\ContentNegotiator',
			'formats' => [
				'application/json' => Response::FORMAT_JSON,
			]
        ];
        $behaviors['authenticator'] = [
			'class' => HttpBearerAuth::className(),
            'except' => ['create', 'confirm', 'login', 'login-courier'],
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']   = 'api\modules\v1\controllers\actions\UserIndexAction';
        $actions['create']  = 'api\modules\v1\controllers\actions\UserCreateAction';
        $actions['confirm'] = 'api\modules\v1\controllers\actions\UserConfirmAction';
        $actions['login']   = 'api\modules\v1\controllers\actions\UserLoginAction';
        $actions['login-courier']   = 'api\modules\v1\controllers\actions\UserLoginCourierAction';
        $actions['logout']  = 'api\modules\v1\controllers\actions\UserLogoutAction';
        $actions['fcm-token']  = 'api\modules\v1\controllers\actions\UserFcmTokenAction';
		$actions['profile'] = 'api\modules\v1\controllers\actions\UserProfileAction';
        $actions['address'] = 'api\modules\v1\controllers\actions\UserAddressAction';
        return $actions;
    }
}
