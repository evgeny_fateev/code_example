<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'carry_food-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'uk',
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'controllerNamespace' => 'api\common\controllers',
    'components' => [
        'request' => [
            'baseUrl' => '/api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,

        ],
        'response' => [
            'charset' => 'UTF-8',
            'format' => yii\web\Response::FORMAT_JSON,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'enableAutoLogin' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'common\components\TelegramLogTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:401'
                    ]
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                // /users
                ['class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/user',
                    'extraPatterns' => [
                        'POST confirm'          => 'confirm',
                        'POST login'            => 'login',
                        'POST login-courier'    => 'login-courier',
                        'POST logout'           => 'logout',
                        'POST fcm-token'        => 'fcm-token',
                        'POST profile'          => 'profile',
                        'POST address'          => 'address',
                    ],
                    'except' => ['update', 'delete', 'options', 'view']
                ],

                // /agent-pay-types
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/agent-pay-type',
                    'except' => ['create', 'update', 'delete', 'options']],

                // agent category
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/agent-category',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /agents
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/agent',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /product-categories
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/product-category',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /products
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/product',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /promotion-banner
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/promotion-banner',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /promotion
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/promotion',
                    'except' => ['create', 'update', 'delete', 'options']],

                // /cart
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/cart',
                    'patterns' => [
                        'DELETE' => 'delete',
                        'POST'   => 'create',
                        'GET'    => 'index'
                    ],
                    'except' => ['update', 'options']
                ],

                // /order
                ['class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/order',
                    'extraPatterns' => [
                        'GET active' => 'active',
                        'POST rating' => 'rating',
                        'POST cancel' => 'cancel'
                    ],
                    'except' => ['options', 'update', 'delete']
                ],

                // /promo code
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/promo-code',
                    'except' => ['index', 'update', 'delete', 'options']],

                // /question-answer
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/question-answer',
                    'except' => ['update', 'delete', 'options']],

                // /courier
                ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/courier',
                    'extraPatterns' => [
                        'POST status' => 'status',
                        'GET active-order' => 'active-order',
                        'POST status-order' => 'status-order',
                        'GET orders' => 'orders'
                    ],
                    'except' => ['index', 'create', 'update', 'delete', 'options']],
            ],
        ],
    ],
    'params' => $params,
];
