<?php


namespace common\models\forms;

use common\models\UserToken;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * @property string $phone
 * @property string $message
 *
 * @property User $_user
 */
class LoginForm extends Model
{
    public $phone;
    public $message;

    private $_user;

    const SCENARIO_USER = 1;
    const SCENARIO_COURIER = 2;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_USER        => ['phone', 'message'],
            self::SCENARIO_COURIER     => ['phone', 'message'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            ['phone', 'string', 'max' => 12],
            ['phone', 'match', 'pattern' => '/^380\d{9}$/'],
            ['phone', 'validatePhone'],
            ['message', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        // remove fields that contain sensitive information
        unset($fields['fcm_token'], $fields['_user']);

        return $fields;
    }

    /**
     * Function validate phone
     *
     * @param $attribute
     */
    public function validatePhone($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Невірний номер телефону');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон'
        ];
    }

    /**
     * Function login user
     *
     * @return $this|null
     */
    public function login()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = $this->_user;

        if ($this->scenario == self::SCENARIO_COURIER && empty($user->courier)){
            $this->addError('phone', 'Ви не зареєстровані як курєр');
            return null;
        }

        $user_token = UserToken::findOne(['user_id' => $user->id]);
        if (!empty($user_token)){
            if ($user_token->type == UserToken::TYPE_LOGIN && (time() - $user_token->updated_at <= 3 * 60)) {
                $this->addError('phone', 'Відправка СМС дозволена раз в 3 хвилини');
                return null;
            }
        }
        else{
            $user_token = new UserToken();
        }

        $user_token->user_id = $user->id;
        $user_token->generateCode();
        $user_token->code = 1234;
        $user_token->type = UserToken::TYPE_LOGIN;
        $user_token->updated_at = time();
        if ($user_token->save()){
            $this->message = 'смс успішно відправлено';
            return $this;
        }
        return null;
    }

    /**
     * Finds user by [[phone]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByPhone($this->phone);
        }
        return $this->_user;
    }
}
