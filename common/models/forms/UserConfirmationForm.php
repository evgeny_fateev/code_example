<?php

namespace common\models\forms;

use common\models\UserToken;
use Yii;
use yii\base\Model;
use common\models\TempUser;
use common\models\User;

/**
 * User confirmation form
 *
 * @property string $phone
 * @property integer $code
 * @property string $message
 * @property string scenario_confirm
 */
class UserConfirmationForm extends Model
{
    public $phone;
    public $code;
    public $message;

    public $scenario_confirm;

    public $fcm_token;

    const CONFIRM_REGISTRATION = 'registration';
    const CONFIRM_LOGIN = 'login';
    const CONFIRM_UPDATE_PHONE = 'update_phone';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'code', 'scenario_confirm'], 'required'],
            [['phone', 'code'], 'trim'],
            ['phone', 'string', 'max' => 12],
            ['phone', 'match', 'pattern' => '/^380\d{9}$/'],
            ['code', 'integer'],
            ['fcm_token', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        // remove fields that contain sensitive information
        unset($fields['phone'], $fields['code']);

        return $fields;
    }

    /**
     * Confirm new user
     *
     * @return User|null
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function confirm(){
        if (!$this->validate()) {
            return null;
        }

        $tmp_user = TempUser::findByPhone($this->phone);
        if (empty($tmp_user)){
            $this->addError('phone', 'Нема знайдено');
            return null;
        }
        elseif ($tmp_user->code != $this->code){
            $this->addError('code', 'Невірний код підтвердження');
            return null;
        }
        elseif (time() - $tmp_user->timestamp > 3 * 60){
            $this->addError('code', 'Код підтвердження закінчився');
            return null;
        }

        $user = new User();
        $user->phone = $tmp_user->phone;
        $user->username = $tmp_user->username;
        $user->email = $tmp_user->email;
        $user->generateAccessToken();
        $user->last_Login_at = time();
        $user->user_ip = Yii::$app->request->getUserIP();
        $user->fcm_token = empty($this->fcm_token) ? null : $this->fcm_token;
        $user->birthday = empty($tmp_user->birthday) ? null : $tmp_user->birthday;
        $user->points = 0;
        if ($user->save()){
            $tmp_user->delete();
            return $user;
        }
        return null;
    }

    /**
     * Confirm login user
     *
     * @return User|null
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function confirmLogin(){
        if (!$this->validate()) {
            return null;
        }

        $user = User::findByPhone($this->phone);
        $user_token = $user->token;
        if (empty($user) || empty($user_token)){
            $this->addError('phone', 'Нема знайдено');
            return null;
        }
        elseif ($user_token->code != $this->code){
            $this->addError('code', 'Невірний код підтвердження');
            return null;
        }
        elseif (time() - $user_token->updated_at > 3 * 60){
            $this->addError('code', 'Код підтвердження закінчився');
            return null;
        }

        $user = $user_token->user;
        $user->generateAccessToken();
        $user->last_Login_at = time();
        $user->user_ip = Yii::$app->request->getUserIP();
        $user->fcm_token = empty($this->fcm_token) ? null : $this->fcm_token;
        if ($user->save()){
            $user_token->delete();
            return $user;
        }
        return null;
    }

    /**
     * Confirm change phone
     *
     * @return User|null
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function confirmChangePhone(){
        if (!$this->validate()) {
            return null;
        }

        $user = User::findByPhone($this->phone);

        $user_token = $user->token;
        if (empty($user) || empty($user_token) || $user_token->type != UserToken::TYPE_CHANGE_PHONE){
            $this->addError('phone', 'Нема знайдено');
            return null;
        }
        elseif ($user_token->code != $this->code){
            $this->addError('code', 'Невірний код підтвердження');
            return null;
        }
        elseif (time() - $user_token->updated_at > 3 * 60){
            $this->addError('code', 'Код підтвердження закінчився');
            return null;
        }

        $user->phone = $user_token->new_phone;
        $user->change_phone_at = time();
        if ($user->save()){
            $user_token->delete();
            return $user;
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code'    => 'Код',
            'phone'   => 'Телефон'
        ];
    }
}
