<?php


namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property    integer     $id
 * @property    integer     $status
 * @property    boolean     $is_call
 * @property    string      $birthday
 * @property    integer     $points
 * @property    integer     $last_Login_at
 * @property    integer     $change_phone_at
 * @property    integer     $created_at
 * @property    integer     $updated_at
 * @property    integer     $is_change_phone
 *
 *
 * @property    string      $auth_key
 * @property    string      $access_token
 * @property    string      $username
 * @property    string      $email
 * @property    string      $phone
 * @property    string      $fcm_token
 *
 * @property    string      $user_ip
 *
 * @property string $password write-only password
 * @property UserToken $token
 * @property UserAddress $address
 * @property UserPromoCode $promoCode
 * @property array $calcCart
 * @property UserCard $card
 * @property UserCourier $courier
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED    = 0;
    const STATUS_ACTIVE     = 10;

    public $is_change_phone;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_token', 'username', 'phone'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['username', 'phone'], 'string'],
            [['points'], 'integer'],
            [['email'], 'email'],
            [['is_call'], 'boolean'],
            ['birthday', 'birthdayValidator'],
            ['auth_key', 'string', 'max' => 32],
            ['is_change_phone', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'auth_key'              => 'Auth key',
            'status'                => 'Статус',
            'is_call'               => 'Колл-центр',
            'created_at'            => 'Создан',
            'updated_at'            => 'Обновлён',
            'username'              => 'Имя',
            'email'                 => 'E-mail',
            'phone'                 => 'Телефон',
            'points'                => 'Баллы',
            'birthday'              => 'Дата рождения',
            'access_token'          => 'Токен доступа',
            'last_Login_at'         => 'Время последней авторизации',
            'change_phone_at'       => 'Время смена номера телефона',
            'user_ip'               => 'IP адрес'
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        // remove fields that contain sensitive information
        unset($fields['status'], $fields['password_reset_token'], $fields['last_Login_at'],
            $fields['created_at'], $fields['updated_at'], $fields['fcm_token'], $fields['user_ip'],
            $fields['auth_key'], $fields['is_call'], $fields['change_phone_at']
        );

        if (isset($this->is_change_phone)){
            $fields['is_change_phone'] = function(){
                return boolval($this->is_change_phone);
            };
        }

        $fields['address'] = function (){
            return $this->address;
        };

        $fields['cards'] = function (){
            return $this->card;
        };

        if ($this->courier != null){
            $fields['courier'] = function (){
                return $this->courier;
            };
        }

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return ['promo'];
    }

    /**
     * Generates access token
     * This is sha1 hash sum of username:password_hash:TIME
     */
    public function generateAccessToken()
    {
        $this->access_token = hash('sha256', $this->phone . rand(100, 9999) . time());;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $key the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($key)
    {
        return $this->getAuthKey() === $key;
    }

    /**
     * Finds user by the given token.
     *
     * @param string $token the token to be looked for
     * @param $type
     * @return User|null object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by phone
     *
     * @param string $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::findOne([
            'phone' => $phone,
            'status' => self::STATUS_ACTIVE
        ]);
    }


    /**
     * Validates birthday
     *
     * @param string $birthday birthday to validate
     * @return boolean if birthday provided is valid for current user
     */
    public function birthdayValidator() {
        if (\DateTime::createFromFormat('Y-m-d', $this->birthday) === false) {
            $this->addError("birthday", "Неверный формат");
            return false;
        }

        return true;
    }


    /**
     * Changes response: boolean as true and false
     */
    public function afterFind() {
        $this->is_call = ($this->is_call === 1);
        parent::afterFind();
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getToken(){
        return $this->hasOne(UserToken::className(), ['user_id' => 'id']);
    }


    public function getOrders()
    {
        return $this->hasMany(Order::classname(), ['user_id' => 'id']);
    }

    public function getStatusList()
    {
        return [self::STATUS_ACTIVE => 'Активен', self::STATUS_DELETED => 'Заблокирован'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(UserAddress::classname(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode(){
        return $this->hasOne(UserPromoCode::classname(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard(){
        return $this->hasMany(UserCard::classname(), ['user_id' => 'id']);
    }

    public function getCourier(){
        return $this->hasOne(UserCourier::className(), ['user_id' => 'id']);
    }

    /**
     * Function calculation cart from user
     *
     * @return array|bool
     */
    public function calcCart(){
        $carts = (new Query())
            ->select([
                'SUM(c.total_price) total_price',
                'SUM(c.quantity) total_quantity',
                'SUM(c.packing_cost) packing_cost_price',
                'c.agent_id agent_id',
                'SUM(
                    CASE 
                        WHEN c.discount = "'.ProductPromotion::PROMO_DISCOUNT.'" THEN 0
                        ELSE c.total_price
                    END
                ) total_price_without_promotion,
                SUM(
                    CASE 
                        WHEN c.discount = "'.ProductPromotion::PROMO_DISCOUNT.'" THEN c.total_price
                        ELSE 0
                    END
                ) total_price_with_promotion
                ',
                'GROUP_CONCAT(c.product_id) product_ids'
            ])
            ->from(['c' => 'cart'])
            ->where(['user_id' => \Yii::$app->user->getId()])
            ->one();
        return $carts;
    }
}
