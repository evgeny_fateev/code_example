<?php


namespace common\models;


use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class UserCourier
 * @package common\models
 *
 * @property int $id
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 *
 * @property string $auto
 * @property string $avatar
 *
 * @property User $user
 */
class UserCourier extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_BREAK = 2;

    static $_status = [
        self::STATUS_ACTIVE => ['text' => 'На смене', 'class' => 'label-success'],
        self::STATUS_INACTIVE => ['text' => 'Не работает', 'class' => 'label-danger'],
        self::STATUS_BREAK => ['text' => 'На перерыве', 'class' => 'label-warning']
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_courier}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'auto', 'avatar'], 'required'],
            [['auto', 'avatar'], 'string', 'max' => 255],
            [['user_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['user_id'], 'exist', 'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'], 'message' => 'Неверный пользователь'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_BREAK]],
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['avatar'] = function(){
            $image_path = empty($this->avatar) ? ('/images/courier/none.jpg') : ('/images/courier/' . $this->avatar);
            return Yii::$app->urlManager->getHostInfo() . $image_path;
        };
        $fields['name'] = function (){
            return $this->user->username;
        };
        $fields['phone'] = function (){
            return $this->user->phone;
        };

        if (Yii::$app->user->identity->courier){
            $fields['profit_today'] = function (){
                return $this->getProfitToday();
            };
        }
        unset($fields['id'], $fields['user_id'], $fields['created_at'],
            $fields['updated_at']);
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'auto' => 'Автомобиль',
            'avatar' => 'Аватар',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
        ];
    }

    public function getProfitToday(){
        $result = (new Query())
            ->from(['ouc' => 'order_user_courier'])
            ->where(['ouc.status' => OrderUserCourier::STATUS_DELIVERED])
            ->andWhere(['BETWEEN', 'ouc.updated_at',
                strtotime('today midnight'),
                strtotime('today +1day midnight')
            ])
            ->sum('ouc.profit_price');
        return doubleval($result);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}