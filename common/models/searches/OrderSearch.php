<?php


namespace common\models\searches;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * Search model for Order
 */
class OrderSearch extends Order
{
    public $status;

    const SCENARIO_USER_SEARCH = 1;
    const SCENARIO_USER_COURIER = 2;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_USER_SEARCH   => ['status'],
            self::SCENARIO_USER_COURIER  => ['status']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->setAttributes($params);

        $query = Order::find();
        if ($this->scenario == self::SCENARIO_USER_COURIER){
            $query->joinWith(['order_status', 'courier'])
                ->where(['order_user_courier.user_courier_id' => Yii::$app->user->identity->courier->id]);
        }
        else{
            $query->joinWith(['order_status'])
                ->where(['user_id' => Yii::$app->user->getId()]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'order_status.status', $this->status]);
        $query->orderBy(['id' => SORT_DESC]);
        return $dataProvider;
    }
}
