<?php


namespace common\models\searches;


use Yii;
use yii\data\ActiveDataProvider;
use common\models\Agent;
use yii\db\Expression;

/**
 * Represents the sarch model for Agent
 */
class AgentSearch extends Agent
{
	public $name;
    public $open_from;
    public $close_to;
    public $delivery_price;
    public $available;
	public $min_order_price;
	public $max_order_price;
    public $agent_category_id;
    public $pay_type_id;
	public $product_category_id;
	public $discount_product;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'open_from', 'close_to', 'delivery_price', 'min_order_price', 'max_order_price',
                'available', 'agent_category_id', 'pay_type_id', 'product_category_id', 'discount_product'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->setAttributes($params);

        if ($this->agent_category_id == '' && $this->product_category_id != '') {
            $join = ['products.product_categories_junction', 'agent_pay_types_junction'];
        } elseif ($this->agent_category_id != '' && $this->product_category_id == '') {
            $join = ['agent_categories_junction', 'agent_pay_types_junction'];
        } else {
            $join = ['agent_categories_junction', 'agent_pay_types_junction'];
        }

        $expand = Yii::$app->request->get('expand');
        if ($expand && in_array('product_promotions', explode(',', $expand))){
            array_push($join, 'product_promotions');
        }

        array_push($join, 'location');

        $query = Agent::find()
            ->select(['agent.*', 'get_opening_agent(agent.open_time, agent.close_time) opening'])
            ->joinWith($join)->distinct();
        $user_address = Yii::$app->user->identity->address;

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query->where(['agent.available' => 1, 'agent.is_delete' => 0]),
            'pagination' => [
                'pageSize' => 20,
                'validatePage' => false
            ],
        ]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere(['>=', 'agent.open_time', $this->open_from])
            ->andFilterWhere(['<=', 'agent.close_time', $this->close_to])
            ->andFilterWhere(['<=', 'agent.delivery_price', $this->delivery_price])
			->andFilterWhere(['>=', 'agent.min_order_price', $this->min_order_price])
			->andFilterWhere(['<=', 'agent.min_order_price', $this->max_order_price])
        	->andFilterWhere(['=', 'agent_category_id', $this->agent_category_id]);


		if (strpos($this->name, ',')) {
			$where = ['or'];
			foreach (explode(',', $this->name) as $name) {
				array_push($where, ['like', 'agent.name', '%' .$name . '%', false]);
			}
			$query->andFilterWhere($where);
		} else {
			$query->andFilterWhere(['like', 'agent.name',  '%' .$this->name . '%', false]);
		}

        // Add filter for each category if multiple
        if (strpos($this->product_category_id, ',')) {
            $where = ['or'];
            foreach (explode(',', $this->product_category_id) as $id) {
                array_push($where, ['=', 'product_category_id', $id]);
            }
            $query->andFilterWhere($where);
        } else {
            $query->andFilterWhere(['=', 'product_category_id', $this->product_category_id]);
        }

        // Add filter for each pay type if multiple
        if (strpos($this->pay_type_id, ',')) {
            $where = ['or'];
            foreach (explode(',', $this->pay_type_id) as $id) {
                array_push($where, ['=', 'pay_type_id', $id]);
            }
            $query->andFilterWhere($where);
        } else {
            $query->andFilterWhere(['=', 'pay_type_id', $this->pay_type_id]);
        }

        if (!empty($user_address)){
            $lat = $user_address->lat;
            $lng = $user_address->lng;
            $query->andWhere("
                CASE
                  WHEN agent_location.id IS NOT NULL THEN (ST_Distance_Sphere(agent_location.point, ST_GeomFromText('POINT($lat $lng)', 4326)) < (agent_location.delivery_radius * 1000))
                  ELSE TRUE
                END");
        }

        $query->orderBy(new Expression('agent.sort IS NULL, agent.sort ASC'));
        $query->orderBy(['opening' => SORT_DESC]);

        return $dataProvider;
    }
}
