<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170524_201442_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(32),
            'access_token' => $this->string(64)->notNull()->unique(),
            'username' => $this->string(255)->notNull(),
            'phone' => $this->string(12)->notNull()->unique(),
            'email' => $this->string()->defaultValue(null),
            'birthday' => $this->date(),
            'points' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'is_call' => $this->boolean()->notNull()->defaultValue(false),
            'last_Login_at' => $this->integer(),
            'change_phone_at' => $this->integer(),
            'fcm_token' => $this->string(255),
            'user_ip' => $this->string(255),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-user-username',
            'user',
            'username'
        );

        $this->createIndex(
            'idx-user-email',
            'user',
            'email'
        );

        $this->createIndex(
            'idx-user-status',
            'user',
            'status'
        );
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
