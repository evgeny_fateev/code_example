<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cart}}`.
 */
class m210409_052734_create_cart_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cart}}', [
            'user_id' => $this->integer()->notNull(),
            'agent_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'basic_price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'quantity' => $this->integer()->defaultValue(1),
            'packing_cost' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'total_price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'discount' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY(user_id, agent_id, product_id)'
        ]);

        $this->createIndex(
            'idx-cart-user_id',
            'cart',
            'user_id'
        );

        $this->createIndex(
            'idx-cart-discount',
            'cart',
            'discount'
        );
        $this->createIndex(
            'idx-cart-agent_id',
            'cart',
            'agent_id'
        );
        $this->createIndex(
            'idx-cart-product_id',
            'cart',
            'product_id'
        );

        $this->addForeignKey(
            'fk-cart-agent_id-agent-id',
            'cart',
            'agent_id',
            'agent',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-cart-product_id-product-id',
            'cart',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-cart-user_id-user-id',
            'cart',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cart}}');
    }
}
